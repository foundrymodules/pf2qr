import {critCheck} from "./crit-test.js";
import {showResults} from "./show-results.js";
import {generateChatMessage} from "./generate-chat-message.js";

let messages = []
let messageCount;

const validSaveTypes = ["reflex", "will", "fortitude"];

export async function forceDCSave(action, dc) {

	messages = [];
	messageCount = [...game.user.targets].length;

    if (!validSaveTypes.includes(action)) {
        ui.notifications.error("Invalid save type. Make sure this particular spell has a save type in its config, not all start with one.")
        return;
    }
	
    dc = parseInt(dc[0]);
    
	[...game.user.targets].map(token => {
		rollTokenSave(token, action, dc);
	});
}

function rollTokenSave(token, action, dc) {
	token.actor.saves[action].check.roll( { skipDialog: !game.user.settings['showRollDialogs'], callback: (result) => {
		//Success step meaning:
		// 3 = Critical
		// 2 = success
		// 1 = failure
		// 0 = critical failure
		let successStep = -1;

		//Rolling the save from the target actor. WILL NOT WORK OF QUICKROLLS ARE NOT ENABLED

		let roll = result

		if (!roll) {
			return '';
		}

		//Setting the basic roll from its comparison with DC:
		if (roll._total >= dc) {
			successStep = 2;
		} else {
			successStep = 1;
		}

		//applying effects of crits and naturals:
		successStep += critCheck(roll, dc);
		successStep = Math.clamp(successStep, 0, 3);

		let successBy = roll._total - dc;

		if (successStep < 0 || successStep > 3) {
			sendMessage(`<div style="color:#FF0000">error. Try again, if that doesn't work, your roll command is invalid. Please report it to @Darth_Apples#4725</div>`);
		}

		sendMessage(generateChatMessage(token, successStep, successBy, false));

		if (game.settings.get("pf2qr", "StopTargetingAfterSave")) token.setTarget(false, { releaseOthers: false, grouSelection: true });
	}});
}

function sendMessage(message) {
	messages.push(message);
	if (messages.length != messageCount) {
		return;
	}
	//finishing message:
	let compiledMessage = "<h3 style='border-bottom: 3px solid black'>Save Results"/* DC" + dc +*/ + ":</h3>" //ADD A CONFIG FOR THIS IN FUTURE PLEASE!
	compiledMessage += messages.join('');

	let chatData = {
		user: game.user.data._id,
		content: compiledMessage	
	}
	showResults(chatData);
}
